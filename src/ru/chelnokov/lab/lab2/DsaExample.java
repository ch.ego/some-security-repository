package ru.chelnokov.lab.lab2;

import java.nio.charset.StandardCharsets;
import java.security.*;

public class DsaExample {
    private static PrivateKey privateKey;
    private static PublicKey publicKey;

    public DsaExample() throws NoSuchAlgorithmException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("DSA");
        generator.initialize(1024, new SecureRandom());
        KeyPair pair = generator.generateKeyPair();
        publicKey = pair.getPublic();
        privateKey = pair.getPrivate();
    }

    byte[] sign(String message, Signature signature) throws InvalidKeyException, SignatureException {
        byte[] bytes = message.getBytes(StandardCharsets.UTF_8);
        signature.initSign(privateKey);
        signature.update(bytes);
        return signature.sign();
    }

    boolean verify(String message, byte[] signed, Signature signature) throws SignatureException, InvalidKeyException {
        signature.initVerify(publicKey);
        signature.update(message.getBytes(StandardCharsets.UTF_8));
        return signature.verify(signed);
    }
}
