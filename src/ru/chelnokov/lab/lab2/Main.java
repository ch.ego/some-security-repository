package ru.chelnokov.lab.lab2;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;

public class Main {
    public static void main(String[] args) throws NoSuchAlgorithmException, SignatureException, InvalidKeyException {
        Signature signature = Signature.getInstance("DSA");
        String message = "hello world";
        DsaExample ex = new DsaExample();
        byte[] signed = ex.sign(message, signature);
        System.out.println(ex.verify(message, signed, signature));
    }
}
