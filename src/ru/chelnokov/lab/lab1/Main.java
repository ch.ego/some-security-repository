package ru.chelnokov.lab.lab1;

import ru.chelnokov.lab.lab1.aes.AesExample;
import ru.chelnokov.lab.lab1.rsa.RsaExample;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Scanner;

public class Main {
    static final String DIVIDER = "--------------------";
    static Scanner reader = new Scanner(System.in);
    static String password = "don't look at this password!";

    public static void main(String[] args) throws NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeySpecException, InvalidKeyException {
        System.out.println("enter the message:");
        String message = reader.nextLine();

        System.out.println(DIVIDER);
        System.out.println("AES");
        String encodedAes = AesExample.encrypt(message, password);
        System.out.println("encoded: " + encodedAes);
        System.out.println("decoded: " + AesExample.decrypt(encodedAes, password));
        System.out.println(DIVIDER);

        System.out.println("RSA");
        RsaExample example = new RsaExample();
        String encodedRsa = example.encrypt(message);
        System.out.println("encoded: " + encodedRsa);
        System.out.println("decoded: " + example.decrypt(encodedRsa));
        System.out.println(DIVIDER);
    }
}
